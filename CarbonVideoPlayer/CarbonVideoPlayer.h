//
//  CarbonVideoPlayer.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 05/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CBVideoPlayer.h"
#import "CBVideoAdvertisingManager.h"

//! Project version number for CarbonVideoPlayer.
FOUNDATION_EXPORT double CarbonVideoPlayerVersionNumber;

//! Project version string for CarbonVideoPlayer.
FOUNDATION_EXPORT const unsigned char CarbonVideoPlayerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <CarbonVideoPlayer/PublicHeader.h>


