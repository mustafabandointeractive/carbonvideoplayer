// Copyright 2013 Google Inc. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#if !defined(__has_feature) || !__has_feature(objc_arc)
#error "This file requires ARC support."
#endif

#import "GMFPlayerOverlayView.h"
#import "GMFResources.h"
#import "UIButton+GMFTintableButton.h"
#import "GMFTopBarView.h"
#import "UIImage+GMFTintableImage.h"
#import "NSBundle+CarbonVideoPlayerBundle.h"
#import "UIImage+CarbonVideoPlayerBundle.h"


@implementation GMFPlayerOverlayView {
    UIActivityIndicatorView *_spinner;
    UIImage *_playImage;
    UIImage *_pauseImage;
    UIImage *_replayImage;
    UIImage* _closeImage;
    UIImage* _playerBarPlayButtonImage;
    UIImage* _playerBarPauseButtonImage;
    NSString *_playLabel;
    NSString *_pauseLabel;
    NSString *_replayLabel;
    UIButton* _closeButton;
    BOOL _isTopBarEnabled;
    CurrentPlayPauseReplayIcon _currentPlayPauseReplayIcon;
}

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        _isTopBarEnabled = YES;
        // Set the images.
        _playImage = [GMFResources playerBarPlayLargeButtonImage];
        _pauseImage = [GMFResources playerBarPauseLargeButtonImage];
        _replayImage = [GMFResources playerBarReplayLargeButtonImage];
        _closeImage = [GMFResources playerCloseImage];
        _playerBarPlayButtonImage = [GMFResources playerBarPlayButtonImage];
        _playerBarPauseButtonImage = [GMFResources playerBarPauseButtonImage];
        // Set the button label strings (for accessibility).
        _playLabel = NSLocalizedStringFromTable(@"Play",
                                                @"GoogleMediaFramework",
                                                nil);
        _pauseLabel = NSLocalizedStringFromTable(@"Pause",
                                                 @"GoogleMediaFramework",
                                                 nil);
        _replayLabel = NSLocalizedStringFromTable(@"Replay",
                                                  @"GoogleMediaFramework",
                                                  nil);
        
        // Create the play/pause/replay button.
        [self showPlayButton];
        
        _closeButton = [[UIButton alloc] init];
        [_closeButton setImage:_closeImage forState:UIControlStateNormal];
        [_closeButton sizeToFit];
        //      [_closeButton setEnabled:YES];
        //      [_closeButton setShowsTouchWhenHighlighted:YES];
        [_closeButton addTarget:self
                         action:@selector(didPressClose:)
               forControlEvents:UIControlEventTouchUpInside];
        
        [self addSubview:_closeButton];
        [self bringSubviewToFront:_closeButton];
        
        _spinner = [[UIActivityIndicatorView alloc]
                    initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
        [_spinner setUserInteractionEnabled:NO];
        [_spinner setIsAccessibilityElement:NO];
        [_spinner sizeToFit];
        [self addSubview:_spinner];
        
        // Player control bar
        _playerControlsView = [[GMFPlayerControlsView alloc] init];
        _playerControlsView.parentGMFPlayerOverlayView = self;
        [self setSeekbarTrackColorDefault];
        [self addSubview:_playerControlsView];
        
        _topBarView = [[GMFTopBarView alloc] init];
        //    [_topBarView setLogoImage:[GMFResources playerBarPlayButtonImage]];
        
        
        [self addSubview:_topBarView];
        
        [self setupLayoutConstraints];
        [self bringSubviewToFront:_closeButton];
    }
    return self;
}

- (void) didPressClose:(id)sender {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(), ^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf.playerControlsView didPressMinimize:sender];
        }
    });
}

- (void)setupLayoutConstraints {
    //[self setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_spinner setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_playerControlsView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_topBarView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [_closeButton setTranslatesAutoresizingMaskIntoConstraints:NO];
    
    NSDictionary *viewsDictionary = NSDictionaryOfVariableBindings(_spinner,
                                                                   _playerControlsView,
                                                                   _topBarView);
    
    // Align spinner to the center X and Y.
    NSArray *constraints =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[_spinner]|"
                                            options:NSLayoutFormatAlignAllCenterX
                                            metrics:nil
                                              views:viewsDictionary];
    
    // Technically this works with just the Y alignment, but xcode will complain about missing
    // constraints, so we add the X as well.
    constraints = [constraints arrayByAddingObject:
                   [NSLayoutConstraint constraintWithItem:_spinner
                                                attribute:NSLayoutAttributeCenterY
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:_spinner.superview
                                                attribute:NSLayoutAttributeCenterY
                                               multiplier:1.0f
                                                 constant:0]];
    constraints = [constraints arrayByAddingObject:
                   [NSLayoutConstraint constraintWithItem:_spinner
                                                attribute:NSLayoutAttributeCenterX
                                                relatedBy:NSLayoutRelationEqual
                                                   toItem:_spinner.superview
                                                attribute:NSLayoutAttributeCenterX
                                               multiplier:1.0f
                                                 constant:0]];
    
    
    // Align controlbar to the center bottom.
    NSDictionary *metrics = @{
                              @"controlsBarHeight": @([_playerControlsView preferredHeight]),
                              @"titleBarheight": @([_topBarView preferredHeight])
                              };
    constraints = [constraints arrayByAddingObjectsFromArray:
                   [NSLayoutConstraint constraintsWithVisualFormat:@"V:[_playerControlsView(controlsBarHeight)]|"
                                                           options:NSLayoutFormatAlignAllBottom
                                                           metrics:metrics
                                                             views:viewsDictionary]];
    constraints = [constraints arrayByAddingObjectsFromArray:
                   [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_playerControlsView]|"
                                                           options:NSLayoutFormatAlignAllBottom
                                                           metrics:nil
                                                             views:viewsDictionary]];
    
    constraints = [constraints arrayByAddingObjectsFromArray:
                   [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[_topBarView(titleBarheight)]"
                                                           options:NSLayoutFormatAlignAllTop
                                                           metrics:metrics
                                                             views:viewsDictionary]];
    constraints = [constraints arrayByAddingObjectsFromArray:
                   [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[_topBarView]|"
                                                           options:NSLayoutFormatAlignAllTop
                                                           metrics:nil
                                                             views:viewsDictionary]];
    
    constraints = [constraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:_closeButton attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil  attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:50.0f]];
    constraints = [constraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:_closeButton attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil  attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0f constant:50.0f]];
    constraints = [constraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:_closeButton attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTop multiplier:1.0f constant:20.0f]];
    constraints = [constraints arrayByAddingObject:[NSLayoutConstraint constraintWithItem:_closeButton attribute:NSLayoutAttributeTrailing relatedBy:NSLayoutRelationEqual toItem:self attribute:NSLayoutAttributeTrailing multiplier:1.0f constant:-20.0f]];
    
    [self addConstraints:constraints];
}

- (void)setDelegate:(id<GMFPlayerControlsViewDelegate>)delegate {
    _delegate = delegate;
    [_playerControlsView setDelegate:delegate];
}

- (void)showSpinner {
    [_spinner startAnimating];
    [_spinner setHidden:NO];
}

- (void)hideSpinner {
    [_spinner stopAnimating];
    [_spinner setHidden:YES];
}

- (void)setPlayerBarVisible:(BOOL)visible {
    [_closeButton setTintColor:[UIColor whiteColor]];
    [_closeButton setBackgroundColor:[UIColor clearColor]];
    [_topBarView setAlpha:(_isTopBarEnabled && visible) ? 1 : 0];
    [_playerControlsView setAlpha:visible ? 1 : 0];
    [_closeButton setAlpha:(_isTopBarEnabled && visible)  ? 1 : 0];
    
    
    [self setNeedsLayout];
    [self layoutIfNeeded];
}

- (void)disableTopBar {
    _isTopBarEnabled = NO;
    [_topBarView setAlpha:0];
}
- (void)enableTopBar {
    _isTopBarEnabled = YES;
    [_topBarView setAlpha:1];
}

- (void)setPlayPauseResetButtonBackgroundColor:(UIColor *)playPauseResetButtonBackgroundColor {
    _playPauseResetButtonBackgroundColor = playPauseResetButtonBackgroundColor;
    [_closeButton setBackgroundColor:playPauseResetButtonBackgroundColor];
}

- (void)addActionButtonWithImage:(UIImage *)image
                            name:(NSString *)name
                          target:(id)target
                        selector:(SEL)selector {
    [_topBarView addActionButtonWithImage:image name:name target:target selector:selector];
}

- (void)setVideoTitle:(NSString *)videoTitle {
    [_topBarView setVideoTitle:videoTitle];
}

- (void)setLogoImage:(UIImage *)logoImage {
    [_topBarView setLogoImage:logoImage];
}

- (void)showPlayButton {
    _currentPlayPauseReplayIcon = PLAY;
    [_playerControlsView.playPauseButton setImage:_playerBarPlayButtonImage forState:UIControlStateNormal];
}

- (void)showPauseButton {
    _currentPlayPauseReplayIcon = PAUSE;
    [_playerControlsView.playPauseButton setImage:_playerBarPauseButtonImage forState:UIControlStateNormal];
}

- (void)showReplayButton {
    _currentPlayPauseReplayIcon = REPLAY;
    [_playerControlsView.playPauseButton setImage:_playerBarPlayButtonImage forState:UIControlStateNormal];
}

- (void)setTotalTime:(NSTimeInterval)totalTime {
    [_playerControlsView setTotalTime:totalTime];
    [_playerControlsView updateScrubberAndTime];
}

- (void)setDownloadedTime:(NSTimeInterval)downloadedTime {
    [_playerControlsView setDownloadedTime:downloadedTime];
    [_playerControlsView updateScrubberAndTime];
}

- (void)setMediaTime:(NSTimeInterval)mediaTime {
    [_playerControlsView setMediaTime:mediaTime];
    [_playerControlsView updateScrubberAndTime];
}

- (void)setSeekbarTrackColor:(UIColor *)color {
    [_playerControlsView setSeekbarTrackColor:color];
}

- (void)setSeekbarTrackColorDefault {
    // Light blue
    [_playerControlsView setSeekbarTrackColor:[UIColor colorWithRed:0.08235294117
                                                              green:0.49411764705
                                                               blue:0.98431372549
                                                              alpha:1.0]];
}

- (void) resetMediaTime {
    [_playerControlsView resetMediaTime];
}

- (void)disableSeekbarInteraction {
    [_playerControlsView disableSeekbarInteraction];
}

- (void)enableSeekbarInteraction {
    [_playerControlsView enableSeekbarInteraction];
}

- (void)applyControlTintColor:(UIColor *)color {
    // Tint the images for play, pause, and replay.
    _playImage = [_playImage GMF_createTintedImage:color];
    _pauseImage = [_pauseImage GMF_createTintedImage:color];
    _replayImage = [_replayImage GMF_createTintedImage:color];
    _closeImage = [_closeImage GMF_createTintedImage:color];
    // Tint the play/pause/replay button and the controls view.
    [_closeButton GMF_applyTintColor:color];
    [_playerControlsView applyControlTintColor:color];
}

- (void)didPressPlayPauseReplay:(id)sender {
    // Determine which icon the play/pause/replay button is showing and respond appropriately.
    switch (_currentPlayPauseReplayIcon) {
        case PLAY:
            [self.delegate didPressPlay];
            break;
        case REPLAY:
            [self.delegate didPressReplay];
            break;
        case PAUSE:
            [self.delegate didPressPause];
            break;
        default:
            break;
    }
}

// Check if the tap is over the subviews of the overlay, else let it go to handle taps
// in the aboveRenderingView
-(UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *hitView = [super hitTest:point withEvent:event];
    if (hitView == self) {
        return nil;
    }
    return hitView;
}

- (void)dealloc {
}

- (void) setPlayerControlsViewHidden:(BOOL)val {
    [self.playerControlsView setHidden:val];
}

@end

