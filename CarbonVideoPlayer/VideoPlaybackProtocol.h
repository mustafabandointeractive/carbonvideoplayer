//
//  VideoPlaybackProtocol.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 23/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol VideoPlaybackProtocol <NSObject>

- (void)videoPlaybackDidFinish: (NSNotification*)notification;

@end
