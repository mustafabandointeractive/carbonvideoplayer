//
//  CBVideoAdXStrategy.h
//  Pods
//
//  Created by Tolga Caner on 10/05/16.
//
//

#import <Foundation/Foundation.h>
#import "CBVideoStrategy.h"

@interface CBVideoAdXStrategy : NSObject <CBVideoStrategy> {}

@end
