//
//  CBVideoAdvertisingManager.m
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 26/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import "CBVideoAdvertisingManager.h"
#import <AdSupport/ASIdentifierManager.h>
#import <CoreFoundation/CFUUID.h>
#if __has_include("CBAdManifest.h") && __has_include("CBDFPRequest.h") && __has_include("CBAppDelegate.h")
#else
#import "NSString+SHAOne.h"
#endif

static NSString * const kGeneratedAdvertisingId = @"GeneratedAdvertisingIdentifier";
static NSString* advertisingIdentifierString;
static NSString* fallbackInterstitialTag;
static BOOL useDefaultImaPlayer;

@implementation CBVideoAdvertisingManager

+ (NSString*) advertisingIdentifier {
    if (!advertisingIdentifierString)
        advertisingIdentifierString = [[[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString] sha1FromString];
    
    if (!advertisingIdentifierString) {
        //Means the advertisingIdentifier isn't available
        NSString* previousUUID = [CBVideoAdvertisingManager getAdvertisingUUID];
        if (!previousUUID) {
            NSString *uuid = [[NSUUID UUID] UUIDString];
            [CBVideoAdvertisingManager saveAdvertisingUUID:uuid];
        }
        previousUUID = [CBVideoAdvertisingManager getAdvertisingUUID];
        advertisingIdentifierString = previousUUID;
    }
    return advertisingIdentifierString;
}

+ (void)saveAdvertisingUUID:(NSString*)uuid
{
    [[NSUserDefaults standardUserDefaults] setObject:uuid forKey:kGeneratedAdvertisingId];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (NSString*)getAdvertisingUUID {
    return [[NSUserDefaults standardUserDefaults] stringForKey:kGeneratedAdvertisingId];
}

+ (NSString*)getFallbackInterstitialTag {
    return fallbackInterstitialTag;
}

+ (void)setFallbackInterstitialAdUnitIdWithTag:(NSString*)tag {
    fallbackInterstitialTag = tag;
}

+ (BOOL)shouldUserDefaultImaPlayer {
    return useDefaultImaPlayer;
}

+ (void) configureAppWithFallbackInterstitialAdUnitId:(NSString*)tag {
    fallbackInterstitialTag = tag; 
    [CBVideoAdvertisingManager configure];
}

+ (void) configure {
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"http://carbon.seamlessapi.com/api/player?packagName=%@&version=%@",[[NSBundle mainBundle] bundleIdentifier],[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"]]];
    //
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    
    [NSURLConnection sendAsynchronousRequest:urlRequest queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
     {
         if ([data length] > 0 && error == nil) {
             NSError *serializationError;
             NSMutableDictionary * innerJson = [NSJSONSerialization
                                                JSONObjectWithData:data options:kNilOptions error:&serializationError
                                                ];
             if (serializationError == nil) {
                 NSNumber* booleanVal = [innerJson valueForKey:@"useDefaultImaPlayer"];
                 if (booleanVal) {
                     useDefaultImaPlayer = [booleanVal boolValue];
                 }
             }
         }
     }];
}

@end
