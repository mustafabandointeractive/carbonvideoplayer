//
//  CBVideoAdvertisingManager.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 26/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CBVideoAdvertisingManager : NSObject

+ (void) configure;
+ (void) configureAppWithFallbackInterstitialAdUnitId:(NSString*)tag;

+ (NSString*) advertisingIdentifier;
+ (NSString*)getFallbackInterstitialTag;
+ (void)setFallbackInterstitialAdUnitIdWithTag:(NSString*)tag;
+ (BOOL)shouldUserDefaultImaPlayer;

@end
