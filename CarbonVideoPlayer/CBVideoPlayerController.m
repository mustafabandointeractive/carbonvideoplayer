//
//  CBVideoPlayerController.m
//  Carbon
//
//  Created by Tolga Caner on 13/04/16.
//  Copyright © 2016 mobilike. All rights reserved.
//
#import <AVFoundation/AVFoundation.h>
#import "CBVideoPlayerController.h"
#import "NSBundle+CarbonVideoPlayerBundle.h"
#import "UIImage+CarbonVideoPlayerBundle.h"

typedef enum { PlayButton, PauseButton } PlayButtonType;

@interface CBVideoPlayerController ()

// Tracking for play/pause
@property(nonatomic) BOOL isAdPlayback;

// Play/Pause buttons.
@property(nonatomic, strong) UIImage *playBtnBG;
@property(nonatomic, strong) UIImage *pauseBtnBG;

@end

@implementation CBVideoPlayerController

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        [[self progressBar] addTarget:self action:@selector(videoControlsTouchEnded:) forControlEvents:UIControlEventTouchUpInside];
        [[self progressBar] addTarget:self action:@selector(videoControlsTouchEnded:) forControlEvents:UIControlEventTouchUpOutside];
        [[self progressBar] addTarget:self action:@selector(videoControlsTouchStarted:) forControlEvents:UIControlEventTouchDown];
        // Set the play button image.
        self.playBtnBG = [UIImage imageNamed:@"vp_play" inCarbonVideoPlayerBundle:[NSBundle CVPFrameworkBundle]];
        // Set the pause button image.
        self.pauseBtnBG = [UIImage imageNamed:@"vp_pause" inCarbonVideoPlayerBundle:[NSBundle CVPFrameworkBundle]];
        self.isAdPlayback = NO;
    }
    return self;
}


#pragma mark VideoViewController Portrait

- (IBAction) videoControlsTouchStarted:(id)sender {
[NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hideFullscreenControls) object:self];
}

- (IBAction)videoControlsTouchEnded:(id)sender {
    [self startHideControlsTimer];
}

- (void)showFullscreenControls:(UITapGestureRecognizer *)recognizer {
        self.hidden = NO;
        self.alpha = 0.9;
        [self startHideControlsTimer];
}

- (void)startHideControlsTimer {
    [self performSelector:@selector(hideFullscreenControls) withObject:self afterDelay:3];
}

- (void)hideFullscreenControls {
    [UIView animateWithDuration:0.5 animations:^{ self.alpha = 0.0; }];
}


#pragma mark UI handlers

// Handle clicks on play/pause button.
- (IBAction)onPlayPauseClicked:(id)sender {
    if (self.isAdPlayback == NO) {
        if ([self.videoPlayerDelegate playerRate] == 0) {
            [self.videoPlayerDelegate playCommand];
        } else {
            [self.videoPlayerDelegate pauseCommand];
        }
    } else {
        if (self.playHeadButton.tag == PlayButton) {
            [self.videoPlayerDelegate resumeAdsManager];
            [self setPlayButtonType:PauseButton];
            [self.videoPlayerDelegate hideCentralPlayButton];
        } else {
            [self.videoPlayerDelegate pauseAdsManager];
            [self.videoPlayerDelegate showCentralPlayButton];
            [self setPlayButtonType:PlayButton];
        }
    }
}

// Updates play button for provided playback state.
- (void)updatePlayHeadState:(BOOL)isPlaying {
    [self setPlayButtonType:isPlaying ? PauseButton : PlayButton];
}

// Sets play button type.
- (void)setPlayButtonType:(PlayButtonType)buttonType {
    self.playHeadButton.tag = buttonType;
    [self.playHeadButton setImage:buttonType == PauseButton ? self.pauseBtnBG : self.playBtnBG
                         forState:UIControlStateNormal];
}

// Called when the user seeks.
- (IBAction)playHeadValueChanged:(id)sender {
    if (![sender isKindOfClass:[UISlider class]]) {
        return;
    }
    if (self.isAdPlayback == NO) {
        UISlider *slider = (UISlider *)sender;
        // If the playhead value changed by the user, skip to that point of the
        // content is skippable.
        [self.videoPlayerDelegate seekToTime:CMTimeMake(slider.value, 1)];
    }
}

// Used to track progress of ads for progress bar.
- (void)adDidProgressToTime:(NSTimeInterval)mediaTime totalTime:(NSTimeInterval)totalTime {
    CMTime time = CMTimeMakeWithSeconds(mediaTime, 1000);
    CMTime duration = CMTimeMakeWithSeconds(totalTime, 1000);
    [self updatePlayHeadWithTime:time duration:duration];
    self.progressBar.maximumValue = totalTime;
}

// Get the duration value from the player item.
- (CMTime)getPlayerItemDuration:(AVPlayerItem *)item {
    CMTime itemDuration = kCMTimeInvalid;
    if ([item respondsToSelector:@selector(duration)]) {
        itemDuration = item.duration;
    } else {
        if (item.asset && [item.asset respondsToSelector:@selector(duration)]) {
            // Sometimes the test app hangs here for ios 4.2.
            itemDuration = item.asset.duration;
        }
    }
    return itemDuration;
}

// Updates progress bar for provided time and duration.
- (void)updatePlayHeadWithTime:(CMTime)time duration:(CMTime)duration {
    if (CMTIME_IS_INVALID(time)) {
        return;
    }
    Float64 currentTime = CMTimeGetSeconds(time);
    if (isnan(currentTime)) {
        return;
    }
    self.progressBar.value = currentTime;
    self.playHeadTimeText.text =
    [NSString stringWithFormat:@"%d:%02d", (int)currentTime / 60, (int)currentTime % 60];
    [self updatePlayHeadDurationWithTime:duration];
}

// Update the current playhead duration.
- (void)updatePlayHeadDurationWithTime:(CMTime)duration {
    if (CMTIME_IS_INVALID(duration)) {
        return;
    }
    Float64 durationValue = CMTimeGetSeconds(duration);
    if (isnan(durationValue)) {
        return;
    }
    self.progressBar.maximumValue = durationValue;
    self.durationTimeText.text =
    [NSString stringWithFormat:@"%d:%02d", (int)durationValue / 60, (int)durationValue % 60];
}


@end
