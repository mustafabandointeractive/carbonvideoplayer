//
//  NSBundle+CarbonVideoPlayerBundle.h
//  CarbonVideoPlayer
//
//  Created by Tolga Caner on 06/09/16.
//  Copyright © 2016 Mobilike|Opera Mediaworks. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSBundle (CarbonVideoPlayerBundle)

/**
 Bundle for the carbon video player framework. If the bundle is not loaded, it loads the bundle.
 */
+ (NSBundle *)CVPFrameworkBundle;

@end
