#import <AVFoundation/AVFoundation.h>
#import "CBVideoPlayer.h"
#import <objc/runtime.h>
#import "NSBundle+CarbonVideoPlayerBundle.h"
#import "GMFIMASDKAdService.h"
#import "CBVideoStrategyContext.h"
#import "CBVideoPremiumStrategy.h"
#import "CBVideoAdXStrategy.h"
#import "CBVideoAdvertisingManager.h"

#import <GoogleInteractiveMediaAds/GoogleInteractiveMediaAds.h>

#define kPrerollKey                    @"preroll"
#define kMidrollKey                    @"midroll"
#define kPostrollKey                    @"postroll"
#if __has_include("CBAdManifest.h") && __has_include("CBDFPRequest.h") && __has_include("CBAppDelegate.h")
#import "CBAdManifest.h"
#import "CBDFPRequest.h"
#define kCarbonEnvironment YES
#else
#import "NSString+SHAOne.h"
#endif

@interface CBVideoPlayer () {
    void (^videoPlaybackCompletedBlock)(void);
    void (^videoPlaybackResumedBlock)(void);
    void (^videoPlaybackPausedBlock)(void);
}

@property (nonatomic,assign) BOOL previousStatusBarHidden;
@property(nonatomic,strong) NSString* clickThroughUrl;
@property(nonatomic,assign) BOOL fullClickEnabled;

@property (nonatomic,assign)UIInterfaceOrientation _statusBarOrientationUponInterstitialDismissal;

@property(nonatomic, strong) IMACompanionAdSlot *companionSlotTopLeft;
@property(nonatomic, strong) IMACompanionAdSlot *companionSlotBottomLeft;
@property(nonatomic, strong) IMACompanionAdSlot *companionSlotTopRight;
@property (nonatomic, strong) DFPInterstitial *fallbackInterstitial;
@property (atomic, assign) BOOL completionBlockCalled;
@property (nonatomic, assign) BOOL isVideoPlaybackPausedForPostroll;

//- (void)videoPlaybackDidFinish: (NSNotification*)notification;
- (void) applicationDidBecomeActiveCallback:(NSNotification*)notification;

@end

@implementation CBVideoPlayer

static int IMAperiod = -1;
static NSString* prerollIMATag;
static NSString* midrollIMATag;
static NSString* postrollIMATag;
static NSDictionary* customParameters;

#pragma mark Initializers

+ (instancetype)videoPlayerWithURL:(NSString*)url withCategoryName:(NSString*)name contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl completion: (void (^ __nullable)(void))completion
{
    CBVideoPlayer *videoPlayerInstance = [CBVideoPlayer videoPlayerWithURL:url withCategoryName:name contentId:contentId shareUrl:shareUrl];
    videoPlayerInstance->videoPlaybackCompletedBlock = completion;
    return videoPlayerInstance;
}

+ (instancetype)videoPlayerWithURL:(nonnull NSString*)url customParameters:(nullable NSDictionary*)custParams shareUrl:(nullable NSString*)shareUrl prerollTag:(nullable NSString*)prerollTag midrollTag:(nullable NSString*)midrollTag postrollTag:(nullable NSString*)postrollTag midRollPeriodInMinutes:(nullable NSNumber*)midrollPeriod completion: (void (^ __nullable)(void))completion paused: (void (^ __nullable)(void))paused resumed: (void (^ __nullable)(void))resumed{
    
    if (prerollTag)
        prerollIMATag = prerollTag;
    if (midrollTag)
        midrollIMATag = midrollTag;
    if (postrollTag)
        postrollIMATag = postrollTag;
    if (midrollPeriod)
        IMAperiod = midrollPeriod.intValue;
    if (custParams)
        customParameters = custParams;
    
    CBVideoPlayer* videoPlayerInstance = [CBVideoPlayer videoPlayerWithURL:url withCategoryName:nil contentId:nil shareUrl:shareUrl completion:completion];
    videoPlayerInstance->videoPlaybackCompletedBlock = completion;
    videoPlayerInstance->videoPlaybackPausedBlock = paused;
    videoPlayerInstance->videoPlaybackResumedBlock = resumed;
    return videoPlayerInstance;
}
+ (instancetype)videoPlayerWithURL:(NSString*)url withCategoryName:(NSString*)name contentId:(NSString*)contentId shareUrl:(NSString*)shareUrl ;
{
    CBVideoPlayer *videoPlayerInstance = nil;
    UIStoryboard*  sb = [UIStoryboard storyboardWithName:@"CBVideoPlayer"
                                                  bundle:[NSBundle CVPFrameworkBundle]];
    videoPlayerInstance =[sb instantiateViewControllerWithIdentifier:@"cbVideoPlayer"];
#pragma clang diagnostic push 
#pragma clang diagnostic ignored "-Wunused-variable"
    UIView* tempView = videoPlayerInstance.view;
#pragma clang diagnostic pop
    [videoPlayerInstance.videoPlayerViewController setParentVideoViewController:videoPlayerInstance];
    [[NSNotificationCenter defaultCenter] addObserver:videoPlayerInstance selector:@selector(adServiceDidFailToLoad:) name:@"AdService-loader-failedWithErrorData" object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:videoPlayerInstance
                                            selector:@selector(applicationDidBecomeActiveCallback:)
                                                name:UIApplicationDidBecomeActiveNotification
                                              object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:videoPlayerInstance
                                             selector:@selector(videoPlaybackStateDidChange:)
                                                 name:kGMFPlayerPlaybackStateDidChangeNotification
                                               object:nil];
    
    NSString* preRollTag;
    NSString* postRollTag;
    NSString* midRollTag;
    int period = 0;
    
#ifdef kCarbonEnvironment
    preRollTag = [[CBAdManifest sharedAdManifest] videoAdUnitIdWithCategory:name contentId:contentId shareUrl:shareUrl rollType:kPrerollKey] .publisherTag;
    postRollTag = [[CBAdManifest sharedAdManifest] videoAdUnitIdWithCategory:name contentId:contentId shareUrl:shareUrl rollType:kPostrollKey].publisherTag;
    CBVideoRollAd* midRoll = [[CBAdManifest sharedAdManifest] videoAdUnitIdWithCategory:name contentId:contentId shareUrl:shareUrl rollType:kMidrollKey];
    midRollTag = midRoll.publisherTag;
    period = midRoll.period.intValue;
#endif
    
    if (!preRollTag && prerollIMATag)
        preRollTag = [videoPlayerInstance processPublisherTag:[prerollIMATag copy] shareUrl:shareUrl customParameters:customParameters];
    if (!midRollTag && midrollIMATag)
        midRollTag = [videoPlayerInstance processPublisherTag:[midrollIMATag copy] shareUrl:shareUrl customParameters:customParameters];
    if (!postRollTag && postrollIMATag)
        postRollTag = [videoPlayerInstance processPublisherTag:[postrollIMATag copy] shareUrl:shareUrl customParameters:customParameters];
    if (period == 0 && IMAperiod > 0)
        period = IMAperiod;
    
    [videoPlayerInstance.videoPlayerViewController setMidRollTag:midRollTag midRollFrequency:@(period*60) withPostrollTag:postRollTag];
    [videoPlayerInstance.loadingView setHidden:NO]; //Make sure loading is displayed  before any content (video or ad)
    
    NSURL* newURL;
    if ([url isKindOfClass:[NSString class]])
        newURL = [NSURL URLWithString:(NSString*)url];
    else
        newURL = (NSURL*)url;
    [videoPlayerInstance.videoPlayerViewController loadStreamWithURL:newURL imaTag:preRollTag companionSlots:[videoPlayerInstance companionSlots]];
    
    if (!preRollTag || [preRollTag isEqualToString:@""]) {
        [videoPlayerInstance.loadingView setHidden:YES];
    }
    GMFIMASDKAdService* adService = (GMFIMASDKAdService*) videoPlayerInstance.videoPlayerViewController.adService;
    adService.adEventDelegate = videoPlayerInstance;
    
    return videoPlayerInstance;
}

- (NSString*) processPublisherTag:(NSString*)publisherTag shareUrl:(NSString*)shareUrl customParameters:(NSDictionary*)custParams{
    if (publisherTag) {
        if (![publisherTag isEqualToString:@""]) {
            NSString* urlEnd = @"";
            NSString* urlEndPart = @"%@=%@&";
            for (NSString* key in custParams.allKeys) {
                NSString* singlePart = [NSString stringWithFormat:[urlEndPart copy],key,[custParams objectForKey:key]];
                urlEnd = [urlEnd stringByAppendingString:singlePart];
            }
            
            if (urlEnd.length > 0) {
                urlEnd = [urlEnd substringToIndex:[urlEnd length]-1];
            }
            
            NSString* escaped = [self URLEncodedString:urlEnd];
            if (escaped && ![escaped isEqualToString:@""])
                publisherTag = [publisherTag stringByAppendingString:[NSString stringWithFormat:@"%@%@",@"&cust_params=",escaped]];
            
            //extra precaution to prevent pipe characters in the publisherTag
            if ([publisherTag rangeOfString:@"|" options:NSCaseInsensitiveSearch].length != 0) {
                publisherTag = [publisherTag stringByReplacingOccurrencesOfString:@"|" withString:@"%7C"];
            }
            
            if (shareUrl) {
                NSURL* tempUrl = [NSURL URLWithString:publisherTag];
                NSString * q = [tempUrl query];
                NSArray * pairs = [q componentsSeparatedByString:@"&"];
                NSMutableDictionary * kvPairs = [NSMutableDictionary dictionary];
                for (NSString * pair in pairs) {
                    NSArray * bits = [pair componentsSeparatedByString:@"="];
                    NSString * key = [[bits objectAtIndex:0] stringByRemovingPercentEncoding];
                    NSString * value = [[bits objectAtIndex:1] stringByRemovingPercentEncoding];
                    [kvPairs setObject:value forKey:key];
                }
                
                NSString* partToReplace = [kvPairs objectForKey:@"description_url"];
                if (partToReplace) {
                    NSString* escapedShareUrl = [self URLEncodedString:shareUrl];
                    publisherTag = [publisherTag stringByReplacingOccurrencesOfString:partToReplace withString:escapedShareUrl];
                }
            }
            
            //ppid parameter
            publisherTag = [publisherTag stringByAppendingString:[NSString stringWithFormat:@"&ppid=%@",[CBVideoAdvertisingManager advertisingIdentifier]]];
        }
    }
    return publisherTag;
}

- (NSString *) URLEncodedString:(NSString*)input {
    NSMutableString * output = [NSMutableString string];
    const char * source = [input UTF8String];
    int sourceLen = (int)strlen(source);
    for (int i = 0; i < sourceLen; ++i) {
        const unsigned char thisChar = (const unsigned char)source[i];
        if (false && thisChar == ' '){
            [output appendString:@"+"];
        } else if (thisChar == '.' || thisChar == '-' || thisChar == '_' || thisChar == '~' ||
                   (thisChar >= 'a' && thisChar <= 'z') ||
                   (thisChar >= 'A' && thisChar <= 'Z') ||
                   (thisChar >= '0' && thisChar <= '9')) {
            [output appendFormat:@"%c", thisChar];
        } else {
            [output appendFormat:@"%%%02X", thisChar];
        }
    }
    return output;
}

- (void) applicationDidBecomeActiveCallback:(NSNotification*)notification {
    IMAAdsManager* adsManager = ((GMFIMASDKAdService *)self.videoPlayerViewController.adService).adsManager;
    NSTimeInterval total = [adsManager.adPlaybackInfo totalMediaTime];
    
    if (total == 0.0) {        // Was playing content
        if (self.videoPlayerViewController.playbackState == kGMFPlayerStatePaused)
            [self.videoPlayerViewController play];
    } else {        // Was playing an ad
        if (![adsManager.adPlaybackInfo isPlaying]) {
            [adsManager resume];
        }
    }
    
}

- (void) setLoadingHidden:(BOOL)value {
    [self.loadingView setHidden:value];
}

- (void) adClicked {
    if (!_fullClickEnabled)
        return;
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: self.clickThroughUrl]];
}

- (BOOL)shouldAutorotate {
    return YES;
}

#pragma mark Playback and Video Player methods

- (void)videoPlaybackDidFinish: (NSNotification*)notification {
    __weak typeof(self) weakSelf = self;
    dispatch_async(dispatch_get_main_queue(),^{
        __strong typeof(weakSelf) strongSelf = weakSelf;
        if (strongSelf) {
            [strongSelf.videoPlayerViewController dismissViewControllerAnimated:YES completion:nil];
            if (strongSelf->videoPlaybackCompletedBlock) {
                videoPlaybackCompletedBlock();
                strongSelf.completionBlockCalled = YES;
            }
        }
    });
    
    /** Can access the finish reason like this
     *int finishReason = [[[notification userInfo]
     *objectForKey:kGMFPlayerPlaybackDidFinishReasonUserInfoKey] intValue];
     **/
}

- (void)videoPlaybackStateDidChange: (NSNotification*)notification {
    if (notification.object == nil)
        return;
    
    if ([notification.object isKindOfClass:[CBVideoPlayerViewController class]]) {
        CBVideoPlayerViewController* cbVideoPlayerViewController = (CBVideoPlayerViewController*)notification.object;
        if (cbVideoPlayerViewController.videoPlayer.state == kGMFPlayerStatePaused) {
            if (self->videoPlaybackPausedBlock) {
                videoPlaybackPausedBlock();
            }
        } else if (cbVideoPlayerViewController.videoPlayer.state == kGMFPlayerStatePlaying) {
            if (!self.isVideoPlaybackPausedForPostroll) { //else don't resume
                if (self->videoPlaybackResumedBlock) {
                    videoPlaybackResumedBlock();
                }
            }
        }
    }
}

- (void) videoPlaybackPausedForPostroll {
    if (self->videoPlaybackPausedBlock) {
        self.isVideoPlaybackPausedForPostroll = YES;
        videoPlaybackPausedBlock();
    }
}

- (void)didReceiveAdEvent:(IMAAdEvent *)event {
    if ([event.typeString isEqualToString:@"Loaded"]) {
        [self.loadingView setHidden:NO];
    }
    BOOL _premium = NO;
    BOOL _shouldUseDefaultImaPlayer = NO; //TODO: WHAT TO DO HERE?
#ifdef kCarbonEnvironment
    _shouldUseDefaultImaPlayer = [[[[[CBAdManifest sharedAdManifest] manifestModel] data] videoAdsData] useDefaultImaPlayer];
#else
    _shouldUseDefaultImaPlayer = [CBVideoAdvertisingManager shouldUserDefaultImaPlayer];
#endif
    
    
    if (!_shouldUseDefaultImaPlayer)
        _premium = [event.ad.adTitle rangeOfString:@"opera" options:NSCaseInsensitiveSearch].length != 0;
    
    [self configureForPremiumAdWithEvent:event isPremium:_premium];
    
    GMFIMASDKAdService* adService = (GMFIMASDKAdService*) self.videoPlayerViewController.adService;
    adService.requestingMidOrPostRoll = NO;
}

- (void) adServiceDidFailToLoad: (NSNotification*) notification {
    
    if (self.videoPlayerViewController.dismissUponPostrollFinish) {
        [self videoPlaybackDidFinish:nil];
        return;
    }
    
    IMAAdLoadingErrorData* adErrorData = notification.object;
    GMFIMASDKAdService* adService = (GMFIMASDKAdService*) self.videoPlayerViewController.adService;
    
    if (adErrorData.adError.code == kIMAError_VAST_EMPTY_RESPONSE && !adService.requestingMidOrPostRoll) {
        [self loadFallbackInterstitial];
    } else { //video playback will begin shortly
        [self.loadingView setHidden:YES];
        [self.videoPlayerViewController play];
    }
    adService.requestingMidOrPostRoll = NO;
}

- (void) loadFallbackInterstitial {
    DFPRequest* request;
#ifdef kCarbonEnvironment
    self.fallbackInterstitial = [[DFPInterstitial alloc] initWithAdUnitID:[[CBAdManifest sharedAdManifest] videoFallbackInterstitialAdUnitId]];
    request = [CBDFPRequest request];
#else
    self.fallbackInterstitial = [[DFPInterstitial alloc] initWithAdUnitID:[CBVideoAdvertisingManager getFallbackInterstitialTag]];
    request = [DFPRequest request];
#endif
    
    self.fallbackInterstitial.delegate = self;
    
    NSMutableDictionary* dict = [NSMutableDictionary new];
    [dict addEntriesFromDictionary:request.customTargeting];
    request.customTargeting = dict;
    [self.fallbackInterstitial loadRequest:request];
}

#pragma mark fallbackInterstitial

- (void)interstitialDidReceiveAd:(GADInterstitial *)interstitial{
    [self.videoPlayerViewController pause];
#ifdef kCarbonEnvironment
    CBAppDelegate* appDelegate = (CBAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictDeviceOrientationLandscapeAll = YES;
#endif
    [interstitial presentFromRootViewController:self];
}

- (void)interstitial:(GADInterstitial *)ad didFailToReceiveAdWithError:(GADRequestError *)error{
    [self.loadingView setHidden:YES];
    [self.videoPlayerViewController play];
}

- (void) interstitialWillDismissScreen:(GADInterstitial *)ad{
    
#ifdef kCarbonEnvironment
    CBAppDelegate* appDelegate = (CBAppDelegate*)[UIApplication sharedApplication].delegate;
    appDelegate.restrictDeviceOrientationLandscapeAll = NO;
#endif
    __statusBarOrientationUponInterstitialDismissal = [UIApplication sharedApplication].statusBarOrientation;
    [self.loadingView setHidden:YES];
    [self.videoPlayerViewController play];
}

#pragma UIViewController Hooks

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpContentPlayer];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void) viewWillAppear:(BOOL)animated {
    self.previousStatusBarHidden =  [[UIApplication sharedApplication] isStatusBarHidden];
    [[UIApplication sharedApplication] setStatusBarHidden:YES];
}

- (void) viewWillDisappear:(BOOL)animated {
    [[UIApplication sharedApplication] setStatusBarHidden:self.previousStatusBarHidden];
}

#pragma mark Interface Orientation Methods

- (UIInterfaceOrientationMask)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    BOOL isIPAD = NO;
#ifdef kCarbonEnvironment
    isIPAD = IPAD;
#else
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        isIPAD = YES;
    else
        isIPAD = NO;
#endif
    
    if (isIPAD) {
        UIInterfaceOrientation orientation = [UIApplication sharedApplication].statusBarOrientation;
        if (UIInterfaceOrientationIsLandscape(orientation))
            return orientation;
    } else  if (__statusBarOrientationUponInterstitialDismissal != UIInterfaceOrientationUnknown) {
        UIInterfaceOrientation orientation = __statusBarOrientationUponInterstitialDismissal;
        __statusBarOrientationUponInterstitialDismissal = UIInterfaceOrientationUnknown;
        return orientation;
    }
    return UIInterfaceOrientationLandscapeRight;
}

#pragma mark Content Player Setup

- (void)setUpContentPlayer {
    self.videoPlayerViewController = self.childViewControllers.firstObject;
    [self.videoPlayerViewController initPlayer];
    self.videoPlayerViewController.videoPlaybackDelegate = self;
}

#pragma mark Companions

- (NSMutableArray*) companionSlots {
    NSMutableArray* companions = [NSMutableArray new] ;
    
    self.companionSlotTopLeft = [[IMACompanionAdSlot alloc] initWithView: self.companionTopLeft width:((int)self.companionTopLeft.frame.size.width) height:((int)self.companionTopLeft.frame.size.height)];
    self.companionSlotTopRight = [[IMACompanionAdSlot alloc] initWithView: self.companionTopRight width:((int)self.companionTopRight.frame.size.width) height:((int)self.companionTopRight.frame.size.height)];
    self.companionSlotBottomLeft = [[IMACompanionAdSlot alloc] initWithView: self.companionBottomLeft width:((int)self.companionBottomLeft.frame.size.width) height:((int)self.companionBottomLeft.frame.size.height)];
    [self.companionTopRight setUserInteractionEnabled:YES];
    //    NSLog(@"%f - %f",self.companionTopLeft.frame.size.width, self.companionTopLeft.frame.size.height);
    //    NSLog(@"%f - %f",self.companionTopRight.frame.size.width, self.companionTopRight.frame.size.height);
    //    NSLog(@"%f - %f",self.companionBottomLeft.frame.size.width, self.companionBottomLeft.frame.size.height);
    
    [companions addObject:self.companionSlotTopLeft];
    [companions addObject:self.companionSlotTopRight];
    [companions addObject:self.companionSlotBottomLeft];
    
    return companions;
}

#pragma mark Premium Ad Layout Method
- (void) configureForPremiumAdWithEvent:(IMAAdEvent*)event isPremium:(BOOL)isPremium {
    _fullClickEnabled = isPremium;
    IMAAd *ad = event.ad;
    NSString *url = [ad valueForKey:@"clickThroughUrl"];
    
    self.clickThroughUrl = url;
    if ([event.typeString isEqualToString:@"Started"] || [event.typeString isEqualToString:@"All Ads Completed"]) {
        [self.loadingView setHidden:YES];
    }
    
    CBVideoStrategyContext* videoConfigurationContext = [[CBVideoStrategyContext alloc] init];
    if (isPremium) {
        CBVideoPremiumStrategy* premiumStrategy = [[CBVideoPremiumStrategy alloc] initWithVideoPlayerViewController:self.videoPlayerViewController];
        [videoConfigurationContext setStrategy:premiumStrategy];
    }
    // Uncomment and implement if adx needs additional set-up
    /* * * *
     *****************************************************
     else {
     CBVideoAdXStrategy* adxStrategy = [[CBVideoAdXStrategy alloc] init];
     [context setStrategy:adxStrategy];
     }
     *****************************************************
     * * * * */
    [videoConfigurationContext configure];
}

#pragma mark Dealloc

- (void)dealloc {
    if (!_completionBlockCalled) {
        videoPlaybackCompletedBlock();
        //normally we should set completionBlockCalled to YES but who cares after dealloc?
    }
    videoPlaybackCompletedBlock = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end